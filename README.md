This repository is intended to contain a module for runinng metabolic tasks on a cobra model. 
Eventually, it should be possible to integrate with memote.

To clone as submodule, `git submodule add git@gitlab.com:firot/metabolic-tasks.git metabolic_tasks`

Note: python does not like dashes in variables/module names/ect, but it is the convention for git, so replacing it with a underscore helps.
