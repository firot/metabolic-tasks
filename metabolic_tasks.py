import pandas as pd
import cobra
import numpy as np
import joblib
from os import path
from contextlib import closing
from urllib.request import urlopen


mem = joblib.Memory(path.join(path.dirname(__file__), ".cache"))


@mem.cache
def urlcache(url, data=None):
    """Cache download from URL."""
    with closing(urlopen(url, data)) as f:
        return f.read()


url_tasks = urlcache(
    "https://www.ncbi.nlm.nih.gov/" "pmc/articles/PMC6483243/bin/pcbi.1006867.s005.xlsx"
)


def met_to_bigg(met):
    """
    Convert matlab-cobra syntax to cobrapy-bigg.

    >>> met_to_bigg('glc_D[c]')
    'glc__D_c'
    """
    substitutions = [("]", ""), ("_", "__"), ("[", "_"), ("__B", "_B")]
    for sub in substitutions:
        met = met.replace(*sub)
    return met


def get_task_sheet(tasksheet=url_tasks):
    """
    Get task sheet from file or url

    >>> sheet = get_task_sheet()
    >>> assert isinstance(sheet, pd.core.frame.DataFrame)
    >>> assert sheet.isna().sum().sum() == 0
    """

    try:
        df = pd.read_excel(tasksheet)
    except FileNotFoundError:
        print("Tasksheet file not found")
    df = df.fillna({"IN UB": 1000, "OUT UB": 1000})
    df = df.fillna(0)
    return df


def parse_task_sheet(df=get_task_sheet(url_tasks)):
    """
    Parses a task list excel file into a list of task-dicts.

    The tasksheet is read as a dataframe, and then converted to a list of
    dicts describing the task.

    >>> assert isinstance(parse_task_sheet(), pd.core.frame.DataFrame)

    tasksheet: path to tasksheet, xlsx format
    """

    taskrows = list(np.flatnonzero(df.ID.to_numpy()))
    tasklist = [df.iloc[a:b, :] for a, b in zip(taskrows, taskrows[1:] + [len(df)])]
    cols = ["ID", "SYSTEM", "SUBSYSTEM", "DESCRIPTION", "SHOULD FAIL", "TASK"]
    task_series_list = list()
    for task in tasklist:
        mini_df = task[cols[:-1]].iloc[0, :]
        mini_df["TASK"] = parse(task)
        task_df = task_df._append(mini_df)
    return task_df.reset_index()[cols]


def parse(task):
    """
    Parse a metabolisk task expressed as a list of list of strings.
    Returns a dict with keys IN, OUT, EQU that map reactions to bounds.
    New task if ID else continuation of task

    >>> task = pd.read_excel(url_tasks, nrows = 3)
    >>> task[["ID", "DESCRIPTION", "IN", "OUT"]]
        ID                                        DESCRIPTION       IN       OUT
    0  1.0  Oxidative phosphorylation via NADH-coenzyme Q ...   q10[m]  q10h2[m]
    1  NaN                                                NaN  nadh[m]    nad[m]
    2  NaN                                                NaN     h[m]      h[c]
    >>> got = parse(task)
    >>> want = {'q10_m': (-1.0, -1.0),
    ...         'q10h2_m': (1.0, 1.0),
    ...         'nadh_m': (-1.0, -1.0),
    ...         'nad_m': (1.0, 1.0),
    ...         'h_m': (-5.0, -5.0),
    ...         'h_c': (4.0, 4.0)}
    >>> assert got == want

    """
    d = dict()
    for row in task.index:
        for type, lb, ub in zip(
            ["IN", "OUT"], ["IN UB", "OUT LB"], ["IN LB", "OUT UB"]
        ):
            if task.loc[row, type]:
                coef = -1 if type == "IN" else 1
                bounds = tuple(coef * task.loc[row, [lb, ub]])
                mets = (task.loc[row, type],)
                d.update({met_to_bigg(met): bounds for met in mets})
    return d


def compatible_task(model, task):
    """Are the metabolites in a task found in the model?

    Metabolite identifiers specify both molecule and compartment.

    @param task Dict of metabolite_id: 2-tuple of numbers
    >>> from cobra.io import load_model
    >>> print('skip from here '); model = load_model("textbook") # doctest:+ELLIPSIS
    skip ...

    Task is compatible if all metabolite identifiers (with compartments)
    are in the model.

    >>> compatible_task(model, {'h_e': (-5.0, -5.0), 'h_c': (4.0, 4.0)})
    True

    Compartment not in model:

    >>> compatible_task(model, {'h_m': (-5.0, -5.0), 'h_c': (5.0, 5.0)})
    False

    Metabolite not in model:

    >>> compatible_task(model, {'ham_e': (-5.0, -5.0), 'ham_c': (5.0, 5.0)})
    False

    """
    for met in list(task.keys()):
        try:
            model.metabolites.get_by_id(met)
        except KeyError:
            return False
    return True


def add_boundary(model, met, bounds):
    """
    Add boundary reaction to model

    >>> print('skip from here '); model = cobra.Model('test') # doctest:+ELLIPSIS
    skip ...
    >>> model.add_metabolites([cobra.Metabolite(id='ham')])
    >>> add_boundary(model, 'ham', (-1,1))
    >>> assert len(model.reactions) == 1
    """
    pre = "SK" if sum(bounds) < 0 else "DM"
    r = cobra.Reaction(f"{pre}_{met}_temp")
    model.add_reactions([r])
    r.add_metabolites({met: -1})
    r.bounds = -1000, 1000
    r.bounds = bounds


def run_task(model, task, return_sol=False, pfba=False):
    """
    Run task on model

    @param task Dict of metabolite_id: 2-tuple of numbers
    >>> from cobra.io import load_model
    >>> print('skip from here '); model = load_model("textbook") # doctest:+ELLIPSIS
    skip ...
    >>> run_task(model, {'o2_e': (-1.0, -1.0), 'o2_c': (1.0, 1.0)})
    True
    """
    with model:
        for r in model.boundary:
            r.bounds = 0, 0
        for r in model.reactions:
            if r.lower_bound > 0:
                r.lower_bound = 0
            if r.upper_bound < 0:
                r.upper_bound = 0
        for met, bounds in task.items():
            add_boundary(model, met, bounds)
        sol = model.optimize()
        status = model.optimize().status == "optimal"
        if return_sol:
            if pfba and status:
                return cobra.flux_analysis.pfba(model)
            return sol
        return status


def task_stoichiometry(sol, task):
    """
    Compare an optimal solution with bounds and return differences

    >>> fluxes = pd.Series({'SK_ham_temp':-1., 'DM_eggs_temp': 2})
    >>> sol = cobra.Solution(0., 'optimal', fluxes)
    >>> task = {'ham': (-1., -1.), 'eggs': (1., 1.)}
    >>> task_stoichiometry(sol, task)
    {'ham': (-1.0, -1.0), 'eggs': (1.0, 2.0)}
    """
    new_task = task.copy()
    for met, (lb, ub) in task.items():
        flux = sol.fluxes[[r for r in sol.fluxes.index if f"_{met}_temp" in r]][0]
        if lb <= flux <= ub:
            continue
        new_task[met] = (flux, ub) if flux < lb else (lb, flux)
    return new_task


def modified_bounds(model, task):
    """
    This should return how bounds should be updated to pass.
    >>> from cobra import Metabolite, Reaction
    >>> print('skip from here '); model = cobra.Model('test') # doctest:+ELLIPSIS
    skip ...
    >>> model.add_metabolites([Metabolite(id='ham'), Metabolite(id='egg')])
    >>> r = Reaction('HE')
    >>> model.add_reactions([r])
    >>> r.reaction = 'ham -> egg'
    >>> expectation = {'ham': (-2,-1), 'egg':(2,2)}
    >>> result = modified_bounds(model, {'ham': (-1,-1), 'egg':(2,2)})
    >>> assert result == expectation
    """
    sol = run_task(model, task, True)
    if sol.status == "optimal":
        return False
    test_task = task.copy()
    for met, (lb, ub) in task.items():
        if ub < 0:
            lb = -1000
        if lb > 0:
            ub = 1000
        test_task[met] = (lb, ub)
        new_sol = run_task(model, test_task, True)
        if new_sol.status == "optimal":
            return task_stoichiometry(new_sol, task)
    return False


def missing_metabolites(model, task):
    """Identify missing metabolites in model"""
    missing = list()
    for metabolite in task.keys():
        try:
            model.metabolites.get_by_id(metabolite)
        except KeyError:
            missing.append(metabolite)
    return missing


def run_task_sheet(model, tasks=parse_task_sheet(get_task_sheet(url_tasks))):
    """
    >>> import cobra
    >>> print('skip from here '); model = cobra.Model('test') # doctest:+ELLIPSIS
    skip ...
    >>> assert isinstance(run_task_sheet(model), pd.core.frame.DataFrame)
    """
    ts = tasks.copy()
    compatible = list()
    passed = list()
    mod_bounds = list()
    missing_mets = list()
    for _, row in tasks.iterrows():
        task = row.TASK
        comp = compatible_task(model, task)
        success = run_task(model, task) if comp else False
        bound = modified_bounds(model, task) if (comp and not success) else False
        missing = False if comp else missing_metabolites(model, task)
        compatible.append(comp)
        passed.append(success)
        mod_bounds.append(bound)
        missing_mets.append(missing)
    ts["COMPATIBLE"] = compatible
    ts["PASSED"] = passed
    ts["MOD_BOUNDS"] = mod_bounds
    ts["MISSING_METABOLITES"] = missing_mets
    return ts


# TODO new columns run_task_sheet():
#       missing metabolites: list(str)


def reaction_to_task(reaction: str):
    """
    Parse reaction string and reformulate as task dict
    """


def task_to_reaction(task: dict):
    """
    Reformulate task to reaction

    This will only work if the task is defined with clear bounds
    Which may sound a bit limiting, but this should be quite easy to define, as the
    task should be mass balanced
    """
